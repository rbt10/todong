import {Component } from "@angular/core"

@Component({
  selector : 'my-todo',
  templateUrl: './todo.component.html',
  styleUrls : ['./todo.component.css']
})

export class TodoComponent{

  todos = [
    {
      todoName: " Projet1",
      todoStatus: true,
      solde : 150,
      isModif : false
    },
    {
      todoName: " Projet2",
      todoStatus: false,
      solde : 100,
      isModif : false
    },
    {
      todoName: " Projet",
      todoStatus: true,
      solde : 200,
      isModif : false
    },

  ]

  onChangeStatus(i:number){
    this.todos[i].todoStatus = !this.todos[i].todoStatus
  }

  onChangeIsModif(i:number){
    this.todos[i].isModif = !this.todos[i].isModif
  }
}
