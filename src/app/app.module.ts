import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { TodoComponent } from "./todo/todo.component";
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from "@angular/forms";


@NgModule({

  declarations :[
    AppComponent,
    TodoComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports:[
    BrowserModule,
    FormsModule
  ],
  bootstrap :[AppComponent]
})

export class AppModule{}

